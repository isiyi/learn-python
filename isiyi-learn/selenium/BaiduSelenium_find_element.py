
from selenium import webdriver
from selenium.webdriver.common.by import By
# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'http://www.haha56.net/xiaohua/xiaosi/'
web_driver.get(url)
# 通过id查找元素
# 测试网站： https://baijiahao.baidu.com/s?id=1606562880383280489&wfr=spider&for=pc
# element_by_id = web_driver.find_element_by_id("content-container")
# element_by_id = web_driver.find_element(By.ID,"content-container")
# print(element_by_id.text)

# 通过class查找元素
# 测试网站： https://baijiahao.baidu.com/s?id=1606562880383280489&wfr=spider&for=pc

# by_class_name = web_driver.find_element_by_class_name("recent-article")
# by_class_name = web_driver.find_element(By.CLASS_NAME,"recent-article")
# print(by_class_name.text)

# 通过name属性来查找元素
# 测试网站： https://baijiahao.baidu.com/s?id=1606562880383280489&wfr=spider&for=pc

# element_by_name = web_driver.find_element_by_name("")# 暂未找到对应的页面demo
# element_by_name = web_driver.find_element(By.NAME, "")
# print(element_by_name.text)

# 通过标签名来查找元素
# 测试网站： http://www.haha56.net/xiaohua/xiaosi/
# element_by_tag_name = web_driver.find_element_by_tag_name("dd")
# element_by_tag_name = web_driver.find_element(By.TAG_NAME,"dd")
# print(element_by_tag_name.text)

# 通过Xpath语法获取元素
# 测试网站： http://www.haha56.net/xiaohua/xiaosi/
# element_by_xpath = web_driver.find_element_by_xpath("//div[@class='pleft']//dd")
# element_by_xpath = web_driver.find_element(By.XPATH,"//div[@class='pleft']//dd")
# print(element_by_xpath.text)

# 通过css选择器选择元素
# 测试网站： http://www.haha56.net/xiaohua/xiaosi/
# element_by_css_selector = web_driver.find_element_by_css_selector(".pleft")
# element_by_css_selector = web_driver.find_element(By.CSS_SELECTOR,".preview")
# print(element_by_css_selector.text)

# 关闭当前页面
web_driver.close()
web_driver.quit()
