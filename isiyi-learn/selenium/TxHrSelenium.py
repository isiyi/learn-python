
from selenium import webdriver
from lxml import etree
# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'https://careers.tencent.com/search.html'
web_driver.get(url)
source = web_driver.page_source
html = etree.HTML(source)
l_list = html.xpath("//a[@class='recruit-list-link']")
for a in l_list:
    position = a.xpath("//h4/text()")
    print(position)

