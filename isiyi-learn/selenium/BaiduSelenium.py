
from selenium import webdriver

# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'https://www.baidu.com/'
web_driver.get(url)

# 通过page_source获取页面源码
print(web_driver.page_source)
