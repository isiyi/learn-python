
from selenium import webdriver
from lxml import etree


# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'
class Lagou(object):

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path=driver_path)
        self.url = 'https://www.lagou.com/jobs/list_java?labelWords=&fromSearch=true&suginput='
        self.positions = []

    def run(self):
        self.driver.get(self.url)
        source = self.driver.page_source
        self.parse_list_page(source)

    def parse_list_page(self,source):
        html = etree.HTML(source)
        links = html.xpath("//a[@class='position_link']/@href")
        for link in links:
            self.request_detail_page(link)

    def request_detail_page(self,url):
        self.driver.get(url)
        source = self.driver.page_source
        self.parse_detail_page(source)

    def parse_detail_page(self ,source):
        html = etree.HTML(source)
        company_name = html.xpath("//div[@class='job-name']/h4[@class='company']/text()")
        position_name = html.xpath("//div[@class='job-name']/h2[@class='name']/text()")

        postion ={
            "company_name":company_name,
            "position_name": position_name
        }

        self.positions.append(postion)

        print(postion)

def main():
    lagou = Lagou()
    lagou.run()
    print(lagou.positions)

if __name__ == '__main__':
    main()