
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains

# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'https://www.baidu.com/'
web_driver.get(url)
# 先找到对应的元素
input_tag = web_driver.find_element(By.ID, "kw")
sub_tag = web_driver.find_element(By.ID, 'su')

# 创建事件chain
action = ActionChains(web_driver)
action.move_to_element(input_tag)
action.send_keys_to_element(input_tag,'python')
action.move_to_element(sub_tag)
action.click(sub_tag)

# 执行所有存储在ActionChains中的动作
action.perform()




