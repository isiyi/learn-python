
from selenium import webdriver

# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'https://www.baidu.com/'
web_driver.get(url)
cookies = web_driver.get_cookies()
print(cookies)
# for cookie in cookies:
#     print(cookie)

# 这是获取指定的cookie
cookie = web_driver.get_cookie("domain")

# 删除所有的cookie
web_driver.delete_all_cookies()

# 删除指定的cookie
web_driver.delete_cookie("domain")

print(cookie)
