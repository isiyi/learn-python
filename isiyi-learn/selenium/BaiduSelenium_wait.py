
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'
# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)
# 请求页面
url = 'https://www.baidu.com/'

try:
    element = WebDriverWait(web_driver,10).until(
        EC.presence_of_element_located((By.ID,"myId"))
    )
finally:
    web_driver.quit()

