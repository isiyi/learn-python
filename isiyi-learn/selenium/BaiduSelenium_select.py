
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'http://95yueba.com/'
web_driver.get(url)
# 先找到对应的元素
web_driver_find_element = web_driver.find_element(By.ID, "jumpMenu")
selectTags = Select(web_driver_find_element)


# 根据索引选择
# selectTags.select_by_index(0)
# 根据值选择
selectTags.select_by_value("http://www.95you.com")
# 根据可视的文本选择
# selectTags.select_by_visible_text("95游手机游戏平台")

# 取消所有的选择
selectTags.deselect_all()




