
from selenium import webdriver

import time
# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'

# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path)

# 请求页面
url = 'https://www.baidu.com/'
web_driver.get(url)
# 先找到对应的元素
element_by_id = web_driver.find_element_by_id("kw")
# 填充数据
element_by_id.send_keys("python")


# 使用clear清除输入框输入的数据
# time.sleep(2)  # 睡眠2秒，查询清除效果
# element_by_id.clear()

# 输入数据后，要产生事件搜索
submit = web_driver.find_element_by_id("su")
submit.click()



