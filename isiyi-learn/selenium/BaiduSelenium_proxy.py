
from selenium import webdriver

# 获取chromedriver路径
driver_path = r'D:\developer\pydriver\chromedriver_win32\chromedriver.exe'
options = webdriver.ChromeOptions()
options.add_argument("--proxy-server=http://163.125.220.11:8118")
# 初始一个driver,并指定路径
web_driver = webdriver.Chrome(executable_path=driver_path,chrome_options=options)

# 请求页面
url = 'http://httpbin.org/ip'
web_driver.get(url)

print(web_driver.page_source)
