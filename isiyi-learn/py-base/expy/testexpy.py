
class PrintTable(object):
    def __init__(self):
        print("开始打印9*9乘法表")
        self.print99()


    def print99(self):
        for i in range(10):
            for j in range(10):
                print('%d x %d = %2s' %(j,i,i*j))
            print('\n')

    if __name__ == '__main__':
        pt  = PrintTable()