class PrintTable(object):
    def __init__(self):
        print("开始打印9*9乘法表")
        self.print99()


    def print99(self):
        for i in range(1,10):
            for j in range(1,i+1):
                print('{}x{}={}\t'.format(j, i, i * j), end='')
            print()

if __name__ == '__main__':
    pt  = PrintTable()