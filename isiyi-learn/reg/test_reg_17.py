import re

text = 'apple price is $99 orange is $88'
res = re.search(r'.*(\$\d+).*(\$\d+)',text)
print(res.group()) # apple price is $99 orange is $88
print(res.group(0)) #  apple price is $99 orange is $88
print(res.group(1)) # $99
print(res.group(2)) # $88
print(res.groups()) # ('$99', '$88')