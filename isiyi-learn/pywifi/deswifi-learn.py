import pywifi
import sys
import time
from pywifi import const


def deswifi():
    wifi = pywifi.PyWiFi();
    interfaces = wifi.interfaces()[0]
    print(interfaces.name())
    interfaces.disconnect()
    time.sleep(3)

    pywifi_profile = pywifi.profile()
    pywifi_profile.ssid = "1003"
    pywifi_profile.auth = const.AUTH_ALG_OPEN
    pywifi_profile.akm.appen(const.AKM_TYPE_WPA2PSK)
    pywifi_profile.cipher = const.CIPHER_TYPE_CCMP

    interfaces.remove_all_network_profiles()
    profile = interfaces.add_network_profile(pywifi_profile)

    interfaces.connect(profile)
    time.sleep(10)

    isok = True
    if interfaces.status() == const.IFACE_CONNECTED:
        print("成功连接")
    else:
         print("失败")
    interfaces.disconnect()  # 断开连接
    time.sleep(1)
    return isok

deswifi()