import requests

header = {
"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"
}
url = 'http://www.baidu.com/s'
kw = {"wd": "华为"}
resp = requests.get(url, params=kw)
# 查看相应内容，查询unicode编码格式的数据
print(resp.text)
# 查看相应内容， 查看字节流内容
print(resp.content)
# 查看完整的url地址
print(resp.url)
# 查看头部相应编码
print(resp.encoding)
# 查看相应状态码
print(resp.status_code)
