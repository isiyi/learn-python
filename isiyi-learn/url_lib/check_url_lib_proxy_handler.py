from urllib import request

url = 'http://httpbin.org/ip'
# 非代理模式请求
# resp = request.urlopen(url)
# print(resp.read().decode('utf-8')) # 14.30.13.164

# 代理方式请求
handler = request.ProxyHandler({"https": "123.169.35.80:9999"})
opener = request.build_opener(handler)
req = request.Request(url)
resp = opener.open(req)
print(resp.read())
