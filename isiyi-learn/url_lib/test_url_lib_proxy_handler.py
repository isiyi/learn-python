from urllib import request

url = 'http://www.99kao.com/pachong/1031.html'
# 非代理模式请求
# resp = request.urlopen(url)
# print(resp.read().decode('utf-8'))

# 代理方式请求
handler = request.ProxyHandler({"https": "1.197.16.64:9999"})
opener = request.build_opener(handler)
req = request.Request(url)
resp = opener.open(req)
print(resp.read())
