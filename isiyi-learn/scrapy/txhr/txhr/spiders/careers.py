# -*- coding: utf-8 -*-
import scrapy


class CareersSpider(scrapy.Spider):
    name = 'careers'
    allowed_domains = ['tencent.com']
    start_urls = ['https://careers.tencent.com/search.html']

    def parse(self, response):
        a_list = response.xpath("//a[@class='recruit-list-link']")
        print("_"*10)
        for a in a_list:
            position = a.xpath("//h4/text()")
            print(position)
            print("*"*10)

