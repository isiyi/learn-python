# -*- coding: utf-8 -*-
import scrapy
from yangguang.items import YangguangItem

class YgSpider(scrapy.Spider):
    name = 'yg'
    allowed_domains = ['sun0769.com']
    start_urls = ['http://wz.sun0769.com/index.php/question/questionType?page=']

    def parse(self, response):

        tr_list = response.xpath("//div[@class='greyframe']//table[2]//table//tr")
        for tr in tr_list:
            item = YangguangItem()
            item["title"] = tr.xpath("./td[2]//a[2]/@title").extract_first()
            item["url"] = tr.xpath("./td[2]//a[2]/@href").extract_first()
            item["company"] = tr.xpath("./td[2]//a[3]/text()").extract_first()

            item["status"] = tr.xpath("./td[3]/span/text()").extract_first()
            item["net_name"] = tr.xpath("./td[4]/text()").extract_first()
            item["insert_time"] = tr.xpath("./td[5]/text()").extract_first()

            #抓取详情
            yield scrapy.Request(
                item["url"],
                callback=self.parse_detail,
                meta={"item": item}
            )

        #翻页
        # next_url = response.xpath("//div[@class='pagination']/a[text()='>']/@href").extract_first()
        # if next_url is not None:
        #     yield scrapy.Request(
        #         next_url,
        #         callback=self.parse
        #     )


    def parse_detail(self,response):
        item = response.meta["item"]
        item["content"] = response.xpath("//div[@class='wzy1']/table[2]//tr[1]/td/text()").extract()

        # 将item传递给pipeline
        yield item