# -*- coding: utf-8 -*-
import scrapy


class TxhrSpider(scrapy.Spider):
    name = 'txhr'
    allowed_domains = ['tencent.com']
    start_urls = ['https://careers.tencent.com/search.html']

    def parse(self, response):
        a_list = response.xpath("//a[@class='recruit-list-link']")
        print(a_list)
        print("******************************")
        for div in a_list:
            item = {}
            item["position"] = div.xpath("//h4/text()").extract_first()
            print(item)
            yield item
